package com.jamie.sentinel.app.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.stereotype.Service;

/**
 * @author 594781919@qq.com
 * @date 2019-10-09
 **/
@Service
public class SentinelService {

    @SentinelResource(value = "SentinelService#info", blockHandler = "blockHandler", fallback = "fallbackHandler")
    public String info(String msg) {
        System.out.println("info ...");
        return "info: " + msg;
    }

    public String blockHandler(String msg, BlockException e) {
        System.out.println("block ..." + e.getClass().getName());
        return "block: " + msg;
    }

    public String fallbackHandler(String msg, Throwable e) {
        System.out.println("fallback ..." + e.getMessage());
        return "fallback: " + msg;
    }
}
