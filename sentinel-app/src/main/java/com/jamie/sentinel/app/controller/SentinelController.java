package com.jamie.sentinel.app.controller;

import com.jamie.sentinel.app.service.SentinelService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 594781919@qq.com
 * @date 2019-10-09
 **/
@RestController
public class SentinelController {

    @Resource
    private SentinelService sentinelService;

    @GetMapping("/sentinel")
    public String sentinel() {
        return sentinelService.info("hello");
    }
}
