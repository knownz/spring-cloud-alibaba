package com.jamie.nacos.config.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 594781919@qq.com
 * @date 2019-10-09
 **/
@RefreshScope
@RestController
public class ConfigController {
    @Value("${my.name:}")
    private String name;

    @GetMapping("/name")
    public String getName() {
        return "name: " + name;
    }
}
