package com.jamie.seata.account.service;

import com.jamie.seata.account.dao.AccountDao;
import io.seata.core.context.RootContext;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author 594781919@qq.com
 * @date 2019-10-11
 **/
@Service
public class AccountServiceImpl implements com.jamie.seata.AccountService {

    @Resource
    private AccountDao accountDao;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void debit(String userId, int money) {
        System.out.println("AccountServiceImpl, Xid: " + RootContext.getXID());
        System.out.println("AccountServiceImpl, userId: " + userId + ", money: " + money);
        accountDao.updateMoneyByUserId(userId, money);
    }
}
