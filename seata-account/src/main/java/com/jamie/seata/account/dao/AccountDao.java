package com.jamie.seata.account.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author 594781919@qq.com
 * @date 2019-10-11
 **/
@Repository
public interface AccountDao {

    void updateMoneyByUserId(@Param("userId") String userId, @Param("money") Integer money);
}
