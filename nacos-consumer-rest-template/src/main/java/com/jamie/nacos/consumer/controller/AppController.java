package com.jamie.nacos.consumer.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author 594781919@qq.com
 * @date 2019-10-10
 **/
@RestController
public class AppController {

    @Resource
    private RestTemplate restTemplate;

    @GetMapping("/rest")
    public String rest() {
        return restTemplate.getForObject("http://nacos-provider/port", String.class);
    }
}
