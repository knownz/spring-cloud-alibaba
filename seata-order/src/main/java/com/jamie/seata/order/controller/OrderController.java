package com.jamie.seata.order.controller;

import com.jamie.seata.Order;
import com.jamie.seata.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 594781919@qq.com
 * @date 2019-10-11
 **/
@RestController
public class OrderController {

    @Resource
    private OrderService orderService;

    @GetMapping("/order")
    public Order order(String userId, String commodityCode, int orderCount) {
        return orderService.create(userId, commodityCode, orderCount);
    }
}
