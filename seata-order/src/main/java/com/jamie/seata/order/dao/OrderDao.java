package com.jamie.seata.order.dao;

import com.jamie.seata.Order;
import org.springframework.stereotype.Repository;

/**
 * @author 594781919@qq.com
 * @date 2019-10-11
 **/
@Repository
public interface OrderDao {

    void save(Order order);
}
