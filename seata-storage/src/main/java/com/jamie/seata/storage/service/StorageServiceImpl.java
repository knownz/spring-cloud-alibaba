package com.jamie.seata.storage.service;

import com.jamie.seata.StorageService;
import com.jamie.seata.storage.dao.StorageDao;
import io.seata.core.context.RootContext;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author 594781919@qq.com
 * @date 2019-10-11
 **/
@Service
public class StorageServiceImpl implements StorageService {
    @Resource
    private StorageDao storageDao;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deduct(String commodityCode, int count) {
        System.out.println("StorageService, Xid: " + RootContext.getXID());
        System.out.println("StorageService, commodityCode: " + commodityCode + ", count: " + count);
        storageDao.deduct(commodityCode, count);
    }
}
