package com.jamie.seata.storage.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author 594781919@qq.com
 * @date 2019-10-11
 **/
@Repository
public interface StorageDao {
    void deduct(@Param("commodityCode") String commodityCode, @Param("count") int count);
}
