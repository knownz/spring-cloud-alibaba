package com.jamie.nacos.provider.controller;

import com.jamie.server.AppService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 594781919@qq.com
 * @date 2019-10-08
 **/
@RestController
public class HelloController {

    @Resource
    private AppService appService;

    @GetMapping("/port")
    public String port() {
        return appService.getPort();
    }
}
