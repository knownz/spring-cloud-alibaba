package com.jamie.nacos.provider.service;

import com.jamie.server.AppService;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author 594781919@qq.com
 * @date 2019-10-10
 **/
@org.apache.dubbo.config.annotation.Service
public class AppServiceImpl implements AppService {

    @Value("${server.port}")
    private String port;

    @Override
    public String getPort() {
        return "provider port: " + port;
    }
}
