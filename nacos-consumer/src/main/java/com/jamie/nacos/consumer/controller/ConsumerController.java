package com.jamie.nacos.consumer.controller;

import com.jamie.server.AppService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author 594781919@qq.com
 * @date 2019-10-08
 **/
@RestController
public class ConsumerController {

    @Reference
    private AppService appService;

    @GetMapping("/dubbo")
    public String dubbo() {
        return appService.getPort();
    }
}
