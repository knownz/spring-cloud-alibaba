package com.jamie.seata;

public interface StorageService {

    /**
     * deduct storage count
     */
    void deduct(String commodityCode, int count);
}