package com.jamie.seata;

/**
 * @author 594781919@qq.com
 * @date 2019-10-11
 **/
public interface AccountService {
    void debit(String userId, int money);
}
