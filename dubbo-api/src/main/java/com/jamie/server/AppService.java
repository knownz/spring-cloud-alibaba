package com.jamie.server;

/**
 * @author 594781919@qq.com
 * @date 2019-10-10
 **/
public interface AppService {
    String getPort();
}
